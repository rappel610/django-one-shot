from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm
# Create your views here.


def show_TodoList(request):
    todos = TodoList.objects.all()
    context = {'todos': todos}
    return render(request, 'todos/list.html', context)


def Todo_List_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {"todo": todo, }
    return render(request, 'todos/detail.html', context)


def create_Todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            Todo_list = form.save()
            return redirect("todo_list_detail", id=Todo_list.id)
    else:
        form = TodoForm()
    context = {"form": form, }
    return render(request, "todos/create.html", context)


def edit_Todo_list(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=post)
        if form.is_valid():
            Todo_list = form.save()
            return redirect("todo_list_detail", id=Todo_list.id)
    else:
        form = TodoForm(instance=post)
    context = {"form": form, "post": post}
    return render(request, "todos/edit.html", context)


def delete_Todo_list(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        post.delete()
        return redirect('todo_list_list')

    return render(request, "todos/delete.html")


def create_Todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form, }
    return render(request, "todos/create_item.html", context)


def edit_Todo_item(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoItemForm(instance=post)
    context = {"form": form, "post": post}
    return render(request, "todos/edit_item.html", context)
