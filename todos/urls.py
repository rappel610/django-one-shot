from django.urls import path
from todos.views import (
    show_TodoList,
    Todo_List_detail,
    create_Todo_list,
    edit_Todo_list,
    delete_Todo_list,
    create_Todo_item,
    edit_Todo_item,
)


urlpatterns = [
    path("", show_TodoList, name="todo_list_list"),
    path("<int:id>/", Todo_List_detail, name="todo_list_detail"),
    path("create/", create_Todo_list, name="todo_list_create"),
    path("<int:id>/edit/", edit_Todo_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_Todo_list, name="todo_list_delete"),
    path("items/create/", create_Todo_item, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_Todo_item, name="todo_item_update"),
]
